
 function validar(){
    var nombre = document.getElementById("fNombre");
    var contraseña = document.getElementById("fContraseña");
    var boton = document.getElementById("fBoton");
   
    var longitudNombre = nombre.value.length;
    var longitudContraseña = contraseña.value.length;
    var cerrojo1 = true;
    var cerrojo2 = true;
    
    function validarNombre(){
        if (longitudNombre < 3){
            document.getElementById("nombre-feedback").innerHTML = ("El nombre tiene que tener minimo 3 caracteres.");
            
        }
        else {
            document.getElementById("nombre-feedback").innerHTML = ("");
             return cerrojo1 = false;
            
        }
    }
    validarNombre(); 

    function validarContraseña(){
        if (longitudContraseña < 8){
            document.getElementById("contraseña-feedback").innerHTML = ("La contraseña debe tener mínimo 8 caracteres");
    
        }
        else {
            document.getElementById("contraseña-feedback").innerHTML = ("");
             return cerrojo2 = false;
        }
    }
    validarContraseña(); 

    function validarBoton(){
        
        if (cerrojo2 == false && cerrojo1 == false){

            boton.disabled = false;
            boton.classList = "boton2";
        }
       
        else {
            boton.disabled = true;
            boton.classList = "boton1";
          
        }
       
    }
    validarBoton();
 }
