// Ejercicio 1.
function ejercicio01 (a){
    //función para calcular el área de un cuadrado.
    function areaCuadrado(){
        console.log ('El área del cuadrado es: ' + (a * a));
    }
    areaCuadrado();
    //función para calcular el perímetro de un cuadrado.
    function perimetroCuadrado(){
        console.log('El perímetro del cuadrado es: ' + (a * 4));
    }
    perimetroCuadrado();
    }

//Ejercicio 2.
function ejercicio02(a,b,c){
    var h = Math.sqrt((a * a) + (b * b));
    //función para calcular el área del triangulo.
    function areaTriangulo(){
        console.log ('El área del triangulo es:' + ((b * h)/ 2));
    }
    areaTriangulo();
    //función para calcular el perímetro del triangulo.
    function perimetroTriangulo(){
        console.log('El perímetro del triangulo es: ' + (a + b + c));
    }
    perimetroTriangulo();
}

//Ejercicio 3.

function ejercicio03(a,b){
    //función para calcular el área de un rectangulo.
    function areaRectangulo(){
        console.log ('El área del rectangulo es: ' + (a * b));
    }
    areaRectangulo();
    //función para calcular el perímetro de un cuadrado.
    function perimetroRectangulo(){
        console.log('El perímetro del rectangulo es: ' + (2*(a+b)));
    }
    perimetroRectangulo();
    }

//Ejericicio 4.

function ejercicio04(a,b){
    //función para calcular el área y perimetro de un paralelogramo.
    
   // math.sin sirve para calcular el seno en radianes.
    var seno45 = Math.sin(0.7854);
    //x es un lado del triangulo rectangulo pequeño.
    var x = (seno45 * a);
    //Math.sqrt sirve para  hacer la raiz cuadrada. H es la hipotenusa que es la raiz cuadrada de la suma de los catetos al cuadrado.
    var h = Math.sqrt((a * a) + (x * x));
    function areaParalelogramo(){
        console.log('El área del paralelogramo es: ' + (b * h));
        
    }
    areaParalelogramo();
    function perimetroParalelogramo(){
        console.log('El perímetro del paralelogramo es: ' + 2*(b + a));
    }
    perimetroParalelogramo();
}
//Ejercicio 5.

function ejercicio05(a,D,d){
    function areaRombo(){
        console.log('El área del rombo es: ' + (D * d)/2);
    }
    areaRombo();

    function perimetroRombo(){
        console.log('El perimetro del rombo es: ' + (4 * a));
    }
    perimetroRombo();
}

//Ejercicio 6.

function ejercicio06(a,b,D,d){
    function areaCometa(){
        console.log ('El área de la cometa es: ' + (D * d)/2);
    }
    areaCometa();

function perimetroCometa(){
        console.log('El perímetro de la cometa es: ' + (2*(b+a)));
    }
    perimetroCometa();
}

//Ejercicio 7.

function ejercicio07(a,b,B,c,h){
   function areaTrapecio(){
    console.log ('El área del trapecio es: ' + ((B+b)* h)/2);
   }
   areaTrapecio();

   function perimetroTrapecio(){
    console.log ('El perímetro del trapecio es: ' + (B + b + a + c));
   }
   perimetroTrapecio();
}

//Ejercicio 8.

function ejercicio08 (a,b,n){
    var P = (n * b);

    function areaPregular(){
        console.log ('El área del poligono es: ' + ((P * a) / 2));
       }
    areaPregular();
    
    function perimetroPregular(){
        console.log ('El perímetro del poligono es: ' + (n * b));
       }
    perimetroPregular();
}

//Ejercicio 9.

function ejercicio09(r){
    function areaCirculo(){
        console.log ('El área del circulo es: ' + (Math.PI * (r * r)));
       }
    areaCirculo();
    function perimetroCirculo(){
        console.log ('El perímetro del circulo es: ' + (2* Math.PI * r));
    }
    perimetroCirculo();
}

//Ejercicio 10.

function ejercicio10(R,r){
    function areaCorona(){
        console.log ('El área de la corona circula es: ' + (Math.PI*((R * R)-(r * r))));
    }
    areaCorona();
}

//Ejercicio 11.

function ejercicio11(R,n){
    function areaSectorC(){
        console.log ('El área del sector circular es: ' + ((Math.PI * (R*R) * n) / 360));
    }
    areaSectorC();
}

//Ejercicio 12.

function ejercicio12(R,h){
    function areaCilindro(){
        console.log ('El área del cilindro es: ' + ((2 * Math.PI) * R * (h + R)));
    }
    areaCilindro();

    function volumenCilindro(){
        console.log ('El volumen del cilindro es: ' + (Math.PI * (R * R) * h));
    }
    volumenCilindro();
}

//Ejercicio 13.

function ejercicio13(R,g,h){
    function areaCono(){
        console.log ('El área del cono es: ' + (Math.PI *  R * (R + g)));
    }
    areaCono();

    function volumenCono(){
        console.log ('El volumen del cono es: ' + ((Math.PI * (R * R) * h)/3));
    }
    volumenCono();
    }

//Ejercicio 14.

function ejercicio14(R,r,g,h){
    function areaTroncoC(){
        console.log ('El área del tronco de cono es: ' + ((Math.PI* (g * (r + R) + (r * r) + (R * R)))));
    }
    areaTroncoC();

    function volumenTroncoC(){
        console.log ('El volumen del tronco de cono es: ' + ((Math.PI * h * ((R * R) + (r * r) + (R * r))) / 3));
    }
    volumenTroncoC();
    }

//Ejercicio 15.

function ejercicio15(R){
    function areaEsfera(){
        console.log ('El área de la esfera es: ' + (4 * Math.PI * (R * R)));
    }
    areaEsfera();

    function volumenEsfera(){
        console.log ('El volumen de la esfera es: ' + ((4 * Math.PI * (R * R * R))/ 3));
    }
    volumenEsfera();
    }
//Ejercicio 16.

function ejercicio16(R,h){
    function areaCasqueteE(){
        console.log ('El área del casquete esférico es: ' + ((2 * Math.PI) * R * h));
    }
    areaCasqueteE();

    function volumenCasqueteE(){
        console.log ('El volumen del casquete esférico es: ' + ((Math.PI * (h * h) * ((3 * R) - h)) / 3));
    }
    volumenCasqueteE();
    }

//Ejercicio 17.

function ejercicio17(R,n){
    function areaCuñaE(){
        console.log ('El área de la cuña esférica es: ' + ((4* Math.PI * (R * R) + n))/ 360);
    }
    areaCuñaE();

    function volumenCuñaE(){
        console.log ('El volumen de la cuña esférica es: ' + ((4 * Math.PI * (R * R * R) * n)/3));
    }
    volumenCuñaE();
    }

//Ejercicio 19.

function ejercicio19(a){
    function areaCubo(){
        console.log ('El área del cubo es: ' + (6 * (a * a)));
       }
    areaCubo();
    function volumenCubo(){
        console.log ('El volumen del cubo es: ' + (a * a * a));
    }
    volumenCubo();
}

//Ejercicio 20.
/*
function ejercicio20(a,b,c){
    function areaOrtoedro(){
        console.log ('El área del ortoedro es: ' + (2 * ((a * b) + (a * c) + (b * c))));
       }
    areaOrtoedro();
    function volumenOrtoedro(){
        console.log ('El volumen del otroedro es: ' + (a * b * c));
    }
    volumenOrtoedro();
}

//Ejercicio 21.


function ejercicio21(Ab,P,h,a){

    function areaPrismaRecto(){

        var area = P * (h + a);
        console.log(area);
    }
    areaPrismaRecto();

    function volumenPrismaRecto(){
        var volumen = Ab * h;

        console.log(volumen);
    }
    function volumenPrismaRecto();
}

//Ejercicio 22.

function ejercicio22(P,a,Ab,h){

    function areaPiramide(){
        
        console.log((P * (a + a))/ 2);
    }
    function volumenPiramide(){

        console.log((Ab * h)/3);
    }
}
//Ejercicio 23.
/*function ejercicio23(a){

    function tetraedroRectangular(){
        console.log((Math.sqrt(3))* (a * a));
    }

    function volumenTetraedroRectangular(){

        console.log(((Math.sqrt(2)) *(Math.pow(a,3)))/12);
    }
}

//Ejercicio 24.

function ejercicio24(a){

    function octaedroRegular(){

        console.log((2 * (Math.sqrt(3))* (a*a)));
    }

    function volumenOctaedroRegular(){

        console.log((Math.sqrt(2)*(Math.pow(a,3)))/3);
    }
}*/
//Ejercicios Logica

//Ejercicio 26
function ejercicio26(){

    var tres = 3;

    var cinco = 5;

    var arreglomultiplos3 = [];

    var arreglomultiplos5 = [];

    var sumaMultiplos3 = 0;

    var sumaMultiplos5 = 0;

    for (var i = 1 ; (tres * i) < 1000; i++){
        
        var multiplos3 = (tres * i);
        
        arreglomultiplos3.push(multiplos3);
        
       
    }
    
    for (var j = 1 ; (cinco * j) < 1000; j++){
        
        var multiplos5 = (cinco * j);
        
        arreglomultiplos5.push(multiplos5);
    }

    for (var x = 0; x < arreglomultiplos3.length ; x++){

        
       sumaMultiplos3 += arreglomultiplos3[x];
    }

    for (var y = 0; y < arreglomultiplos5.length ; y++){

        
        sumaMultiplos5 += arreglomultiplos5[y];
    }

    var sumaTotalMultiplos = sumaMultiplos3 + sumaMultiplos5;

    
    console.log ("La suma de todos los multiplos de 3 y 5 por debajo de 1000 es:" + sumaTotalMultiplos);
}

//Ejercicio 27

function fibo(n){

    if ( n <= 1) return 1;
    else return fibo (n -1) + (n -2);
}

function ejercicio27(){

    for (var n = 1; n < 1000; n++){
       var numeroFibo = fibo(n);
       var sumafibo = 0;
        if(numeroFibo % 2 == 0 && sumafibo < 1000){

            sumafibo += numeroFibo;
        }
    }
    console.log (sumafibo);
}

//Ejercicio 28

function factorial(n){

    if (n <= 1) return 1;

    else {
        return n + factorial(n -1);
    }
}
function ejercicio28(){

    for (var n = 1; n < 1000; n++){

        var factoriales = factorial(n);
        var sumaFactoriales = 0;
        if ((factoriales % 7) == 0){

            sumaFactoriales += factoriales;
        }
    }
    console.log(sumaFactoriales);
}

//Ejercicio 29.

function ejercicio29(){

    var numerosPares = [];
    var sumaPares = 0;

    for (var n = 1; n < 10000; n++){
        
        if (n % 2 != 0){
            continue;
        }
        else {
            numerosPares.push(n);
        }
    }
   
    numerosPares.forEach(pares => {
        sumaPares += pares;
    });
        if (sumaPares % 44 !== 0)

        console.log ("La suma de todos los pares menores de 10000 NO es divisible entre 44");

        else {
        
            console.log ("La suma de todos los pares menores de 10000 SI es divisible entre 44");

        }
}


//Ejercicio 30

function ejercicio30(){

    var numerosDivisibles3 = [];
    var productoDivisibles3 = 1;
    for (var i = 1; i < 1000; i++){

        if (i % 3 == 0){

            numerosDivisibles3.push(i);
        }
        else{
            continue;
        }
    }
    numerosDivisibles3.forEach(divisibles => {
        
        productoDivisibles3 *= divisibles;
    });
   
    if (productoDivisibles3 % 7 != 0){

        console.log ("El producto de todos los números divisibles por 3 menores de 1000 NO es divisible por 7");
    }
    else {

        console.log ("El producto de todos los números divisibles por 3 menores de 1000 SI es divisible por 7");
    }
}

//Ejercicio 31

function funcionPrimos(n){
    var  primo = true;
    for (i = 2; i < n; i++){
        if (n % i == 0){
        primo = false;
        return  false;
        }
        
   }
   return primo;
}
function ejercicio31(n){

        
        if (funcionPrimos(n) == true){
            console.log ("El número " + n + " es primo");
        }
        else {
       console.log ("El número " + n + " no es primo");
        }
}

//Ejercicio 32.

function nPrimos(){
    var listaPrimos = [];
    
    for (var n = 2;  n < 1000; n++){
        
        if (funcionPrimos(n) == true){

            listaPrimos.push(n);
        }
    }
    return listaPrimos ;
}

function ejercicio32(){

    console.log(nPrimos());
}

//Ejercicio 33.

function factores(n){
   var arregloFactores = [];
    for (i = 1; i <= n; i++){

        if (n % i == 0){
          arregloFactores.push (i);
        }
    }
    return arregloFactores;
}
function ejercicio33(n){

   
    console.log ("Los factores de " + n + " son: " + factores(n));
}

//Ejercicio34

function ejercicio34(n){

   
    for (var i = 1; i <= n ; i++){
        
        var temporal =  factores(i);
        var arraySumaFactores = [];
        var factor = temporal.map(function (suma) {
            return suma;
        });
        var sumaFactores = factor.reduce(function (total,value) {

            return total + value;
           
        }, 0);
        
        console.log (factor);
        console.log (sumaFactores);
       
        
    //no consigo meter sumaFactores en un Array...
}
}
//  Ejercicio 35
function ejercicio35(n){

    for (var i = 1; i <= n; i++){

        var temporal = factores(i);

        var factor = temporal.map(function (suma){
            return suma;
        });
        
        var pares = factor.filter(function(element) {
            if (element % 2 == 0){
                return element;
            }
        });
    }
    console.log (pares);
}

//Ejercicio 36

function ejercicio36(){

    var n = 60085;
    //cambie el numero porque el que propone el profesor es muy grande y el ordenador se me queda colgado.

    var arrayFactores = factores(n);
    var FactoresPrimos = [];
    
    arrayFactores.forEach(element => {

        if( funcionPrimos(element) == true){

            FactoresPrimos.push(element);

        }
    });
    console.log(FactoresPrimos); 
}


//Ejercicio 37

function mayorEntero(){
    var ArregloEnteros = [3,12,11,56,2,78];
    var EnteroMayor = 0;

    ArregloEnteros.forEach(Entero => {
        
        if (Entero > EnteroMayor){
            EnteroMayor = Entero;
        }

    });
    console.log ("El entero mayor es: " + EnteroMayor);
}
    
function ejercicio37(){
    mayorEntero();
}

//Ejercicio 38

function menorEntero(){

    var ArregloEnteros = [3,12,11,56,2,78];
    var enterosMenor = 0;

    ArregloEnteros.forEach(entero => {
        
        if (enterosMenor ===  0){

            enterosMenor = entero;
        }

        else if  (enterosMenor > entero){
            enterosMenor = entero;
        }

    });
    console.log ("El entero menor  es : " + enterosMenor);
}

function ejercicio38(){

    menorEntero();
}


//Ejercicio39

function sinDuplicados(){

    var arregloDuplicados = [2,33,44,6,17,17,89,44];
    var arregloSinDuplicados = [...new  Set(arregloDuplicados)];

    console.log (arregloSinDuplicados);
}
function ejercicio39(){

    sinDuplicados();
}


//Ejercicio 40.

function duplicados(){

    var arregloDuplicados = [2,33,44,6,17,17,89,44];

    var arregloSinDuplicados = [];

   arregloDuplicados.forEach(elementos => {
        
    if (arregloSinDuplicados.indexOf(elementos) == -1){
        arregloSinDuplicados.push(elementos);
    }
});
console.log (arregloSinDuplicados);
}

function ejercicio40(){
    duplicados();
}

//Ejercicio 41.

    
        function sumaCuadrado(n){
            var sumatorioCuadrados = 0;

            for (var i = 1; i <= n; i++){

                sumatorioCuadrados += (i * i);
            }
            return sumatorioCuadrados;
        }

    
        function cuadradoSuma(n){
            var suma = 0;
            var cuadrado = 0;

            for (var i = 1; i <= n; i++){

                suma += i;
            }
            cuadrado = (suma * suma);
           return cuadrado;
           
        }   
  
        function diferenciaCuadrados(n){
            
            var resultado = cuadradoSuma(n) - sumaCuadrado(n);
            return resultado;
        }

function ejercicio41(n){
    

    console.log("La diferencia entre la suma de cuadrados y el cuadrado de la suma de " + n + " es:" + diferenciaCuadrados(n));
}


//Ejercicio 42 y el 43 es el mismo.
function diferenciasCuadrados(){
    var arregloDeDiferencias = [];
    for (var i = 1; i <= 1000; i++){
       
        var diferencias = diferenciaCuadrados(i);

        arregloDeDiferencias.push(diferencias);
    }

    return arregloDeDiferencias;
}

 function ejercicio42(){

    console.log(diferenciasCuadrados());
 }


 //Ejercicio 44 y 45 es el mismo.

 function ejercicio44(){

    var diferencias = diferenciasCuadrados();
    
    var sumaDiferencias = diferencias.reduce (function (total, value){
        return total + value;
    })

    console.log(sumaDiferencias);
 }

 //Ejercicio 46.

 function ejercicio46(){
   var  ArregloPares = [];
    diferenciasCuadrados();
    
    ArregloDeDiferencias.forEach(element => {

        if (element % 2 == 0){

            ArregloPares.push (element);
        }
        
    });
    console.log (ArregloPares);
 }

 //Ejercicio 47.

 function lugarPrimo(p){

    var primos = nPrimos();

    var posicion = primos[p -1];

    return posicion;
 }

 function ejercicio47(p){

    console.log (lugarPrimo(p));
 }

//Ejercicio 48


 function numeroTriangular(N){

    var nTriangular = 0;

    for (var i = 1; i <= N; i++){

        nTriangular += i;
    }
    
    return nTriangular;
    
 }

 function ejercicio48(N){
     console.log (numeroTriangular(N));
     
 }
 
 //Ejercicio 49

function numerosTriangulares(n){

    var  arregloTriangulares = [];
   
    for (var i = 1; i <= n; i++){
        
        var temporal = numeroTriangular(i);
        arregloTriangulares.push (temporal);

    }
    return arregloTriangulares;
}



function ejercicio49(n){
    
    console.log(numerosTriangulares(n));
}

//Ejercicio 50

function ejercicio50(n){

    var arregloTriangulares = numerosTriangulares(n);

    var paresTriangulares = arregloTriangulares.filter(function(element){
        return element % 2 == 0 ;
    });

    console.log (paresTriangulares);
}

//Ejercicio 51
function ejercicio51(n){

    var arregloTriangulares = numerosTriangulares(n);

    var imparesTriangulares = arregloTriangulares.filter(function(element){
        return element % 2 != 0 ;
    })

    console.log (imparesTriangulares);
}

//Ejercicio 52
function ejercicio52(n){

    var arregloTriangulares = numerosTriangulares(n);
    
    var primosTriangulares = arregloTriangulares.filter(function(element){

        var primo = true;

        for (var i = 2; i < element; i++){
            if (element % i == 0){
                primo = false;
                return false;
            }
        }

        if (primo == true){
            return element;
        }
        
    })
    console.log (primosTriangulares);
}

//Ejercicio 53
function triangularesNoPrimos(n){

    var arregloTriangulares = numerosTriangulares(n);
    
    var noPrimosTriangulares = arregloTriangulares.filter(function(element){

        var primo = true;

        for (var i = 2; i < element; i++){
            if (element % i == 0){
                primo = false;
        
            }
        }

        if (primo != true){
            return element;
        }
        
    })
    return noPrimosTriangulares;
}

function ejercicio53(n){

    console.log (triangularesNoPrimos(n));
}

//Ejercicio 54
function factoresTriangualaresNP(n){

    var arregloTriangulares = triangularesNoPrimos(n);

    var factoresTriangulares = arregloTriangulares.map(function (factores){

        var factor = [];
        for (var i = 1; i <= factores ; i++){

            if ( factores % i == 0){
                factor.push(i);
                
            }
           
        }
        return factor;
    })

    return factoresTriangulares;
}


function ejercicio54(n){

    console. log(factoresTriangualaresNP(n));
}

// Ejercicio 55
function totalTriangularesNP(n){

    var arregloTriangulares = factoresTriangualaresNP(n);
    var arregloSumaTriangulares = [];
    
    arregloTriangulares.forEach(element => {

        var temporal = element.reduce(function (total,value) {

            return total + value;
            
        });
        arregloSumaTriangulares.push(temporal);
    
        });
        
        arregloCuadradoTriangulares = arregloSumaTriangulares.map(function (elemento) {

            return elemento * elemento;
            
        });
        
        return arregloCuadradoTriangulares;
   
}

function ejercicio55(n){

    console.log(totalTriangularesNP(n));
}
       
//ejercicio 56

function ejercicio56(n){

    arrayTriangularesNP = totalTriangularesNP(n);

    sumaTriangularesNp = arrayTriangularesNP.reduce(function (total, value) {

        return total + value;
        
    })

    console.log(sumaTriangularesNp);
}
//Ejercicio57

function extraeDigitos(s){

    var arreglo = s.split("");

    var arregloDefinitivo = arreglo.map(function (elemento) {

        return parseInt(elemento);
        
    })
    return arregloDefinitivo;
}

function ejercicio57(s){

    console.log(extraeDigitos(s));
}

function ejercicio58(s){

    var arreglo = extraeDigitos(s);

    var arregloDefinitivo = arreglo.reduce(function (total,value) {

        return total + value;
        
    })
        console.log (arregloDefinitivo);
}

//Ejercicio59 y el 60 son el mimso.

function contadorLetras(s){

    letras = s.split("");

    var repetidos = {};

    letras.forEach(function(numero){
      repetidos[numero] = (repetidos[numero] || 0) + 1;
    });
    
    return repetidos;
    }


function ejercicio59(s){

    console.log(contadorLetras(s));
}

